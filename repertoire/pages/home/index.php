<?php
/**
 * Created by PhpStorm.
 * User: JROLLI01
 * Date: 15/10/14
 * Time: 12:40
 */


require_once __DIR__."/../../../../app/bootstrap.php";

//page name
$name = basename(__DIR__);

//layout
require_once __DIR__ .'/layout.php';

//sections to add (FIFO)
require_once __DIR__ .'/sections.php.php';

//build all use case
buildPage($name, $sections, $gabarits);


