<?php

return array(
  'footerHelpSection' => array(
    'title' => 'Besoin d\'aide ?',
    'links' => array(
      array(
        'title' => 'Contact',
        'url' => 'contact.html'
        ),
      array(
        'title' => 'FAQ',
        'url' => 'faq.html'
        ),
      array(
        'title' => 'Assistance',
        'url' => 'assistance.html'
        ),
      array(
        'title' => 'Légal',
        'url' => 'legal.html'
        )
      )
    ),
  'footerContextualSection' => array(
    'title' => 'La gamme',
    'links' => array(
      array(
        'title' => 'Citadine et compacte',
        'url' => '#',
        'newWindow' => true
        ),
      array(
        'title' => 'Sportive et cabriolet',
        'url' => '#'
        ),
      array(
        'title' => 'Crossover SUV et 4x4',
        'url' => '#'
        ),
      array(
        'title' => 'Monospace et familiales',
        'url' => '#'
        ),
      array(
        'title' => 'Berlines',
        'url' => '#'
        ),
      array(
        'title' => 'Grandes routières',
        'url' => '#'
        ),
      array(
        'title' => 'Utilitaires et sociétés',
        'url' => '#'
        )
      )
    ),
  'footerNewsletterSection' => array(
    'title' => 'Newsletter',
    'newsletterRegistration' => true,
    'placeholder' => 'Votre e-mail',
    'url' => '#'
    ),
  'footerContactSection' => array(
      'title' => 'Service client',
      'phoneNumber' => '0 970 809 123'
    ),
  'footerSocialSection' => array(
      array(
        'title' => 'Facebook',
        'url' => 'https://www.facebook.com/Peugeot',
        'img' => '../../../img/footer-social-facebook.png'
        ),
      array(
        'title' => 'Twitter',
        'url' => 'https://twitter.com/peugeot',
        'img' => '../../../img/footer-social-twitter.png'
        ),
      array(
        'title' => 'Google +',
        'url' => 'https://plus.google.com/+Peugeot',
        'img' => '../../../img/footer-social-googleplus.png'
        ),
      array(
        'title' => 'Instagram',
        'url' => 'http://instagram.com/peugeot',
        'img' => '../../../img/footer-social-instagram.png'
        )
    ),
  'footerLegalText' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  'footerLegalLinksSection' => array(
    array(
      'title' => 'Informations légales',
      'url' => '#'
      ),
    array(
      'title' => 'Crédits',
      'url' => '#'
      ),
    array(
      'title' => 'Vie privée',
      'url' => '#'
      )
    ),
  'footerSiteMapExpand' => array(
      'title' => 'Plan du site',
      'url' => '#'
    )
);
