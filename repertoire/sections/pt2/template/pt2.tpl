<section class="slice-pt2" data-jsobj="[ { 'obj':'slicePT2' } ]">
  <div class="content slide-dark-bg">
    <div class="cols">
      <div class="col">
        <h1>{$footerHelpSection.title}</h1>
        <ul class="links">
        {foreach $footerHelpSection.links as $link}
          <li><a href="{$link.url}">{$link.title}</a></li>
        {/foreach}
        </ul>
      </div>
      <div class="col">
        <h1>{$footerContextualSection.title}</h1>
        <ul class="links">
        {foreach $footerContextualSection.links as $link}
          <li><a href="{$link.url}" {if isset($link.newWindow)}target="_blank"{/if}>{$link.title}</a></li>
        {/foreach}
        </ul>
      </div>
      <div class="col">
        {if $footerNewsletterSection.newsletterRegistration}
          <h1>{$footerNewsletterSection.title}</h1>
          <form action="{$footerNewsletterSection.url}">
            <input type="email" placeholder="{$footerNewsletterSection.placeholder}">
          </form>

          <h1>{$footerContactSection.title}</h1>
          <p class="phone-number">{$footerContactSection.phoneNumber}</p>
        {else}
          <h1>{$footerContactSection.title}</h1>
          <p>{$footerContactSection.phoneNumber}</p>

          <a href="{$footerNewsletterSection.url}"></a>
        {/if}

        <ul class="social">
        {foreach $footerSocialSection as $social}
          <li><a href="{$social.url}" target="_blank" title="{$social.title}"><img src="{$social.img}" alt="{$social.title}"></a></li>
        {/foreach}
        </ul>
      </div>
    </div>

    <a href="{$footerSiteMapExpand.url}" class="site-map-expand-btn">{$footerSiteMapExpand.title}</a>
    <div class="site-map-expand"></div>
  </div>

  <div class="legal-text">{$footerLegalText}</div>

  <ul class="legal-links">
    {foreach $footerLegalLinksSection as $link}
      <li><a href="{$link.url}">{$link.title}</a></li>
    {/foreach}
  </ul>
</section>
