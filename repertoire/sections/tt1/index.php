<?php
/**
 * Created by PhpStorm.
 * User: JROLLI01
 * Date: 15/10/14
 * Time: 12:40
 */


require_once __DIR__."/../../../../app/bootstrap.php";

//tranche name
$name = basename(__DIR__); //'c1';

//use case
require_once __DIR__ . '/usecase.php';

//layout
require_once __DIR__ .'/layout.php';

//build all use case
buildSection($name, $useCase, $gabarits);

//display
echo file_get_contents(HTML_OUTPUT_PATH.'/sections/'.$name.'/index.html');

