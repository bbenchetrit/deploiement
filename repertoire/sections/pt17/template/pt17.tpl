<section class="slice_pt17">
  <div class="header">
    <div class="logo"></div>
    <h1 class="title">
      <ul class="list_style">
        {foreach $modalLanguage as $langs}
        <li class="lang">{$langs.title}</li>
        {/foreach}
      </ul>
    </h1>
  </div>
  <div class="wrap">
    <ul class="list_lang list_style">
    {foreach $listLanguage as $lang}
      <li class="choice">
        <a href="{$lang.url}" class="lang cta-btn cta-btn-4">{$lang.language}</a>
          <ul class="list_links list_style">
            {foreach $lang.links as $link}
            <li>
              <a href="{$link.url}" class="links">{$link.title}</a>
            </li>
            {/foreach}
          </ul>
      </li>
    {/foreach}
    </ul>
  </div>
  <div class="footer">
  </div>
</section>
