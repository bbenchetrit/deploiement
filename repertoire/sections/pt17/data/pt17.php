<?php

return array(
  'modalLanguage' => array(
    array(
      'title' => 'langue'
    ),
    array(
      'title' => 'lingua'
    ),
    array(
      'title' => 'sprache'
    )
  ),
  'listLanguage' => array(
    array(
      'language' => 'francais',
      'url' => '#',
      'links' => array(
        array(
          'title' => 'Gamme',
          'url' => '#'
        ),
        array(
          'title' => 'Concessions',
          'url' => '#'
        ),
        array(
          'title' => 'Offres et financements',
          'url' => '#'
        ),
        array(
          'title' => 'Après-vente',
          'url' => '#'
        ),
        array(
          'title' => 'Marque et technologie',
          'url' => '#'
        ),
        array(
          'title' => 'Professionnels',
          'url' => '#'
        )
      )
    ),
    array(
      'language' => 'italiano',
      'url' => '#',
      'links' => array(
        array(
          'title' => 'gamma',
          'url' => '#'
        ),
        array(
          'title' => 'Trova un negozio',
          'url' => '#'
        ),
        array(
          'title' => 'professionale',
          'url' => '#'
        ),
        array(
          'title' => 'marchio e tecnologia',
          'url' => '#'
        ),
        array(
          'title' => 'e offre il finanziamento',
          'url' => '#'
        ),
        array(
          'title' => 'servizi e accessori',
          'url' => '#'
        )
      )
    ),
    array(
      'language' => 'deutsche',
      'url' => '#',
      'links' => array(
        array(
          'title' => 'Reichweite',
          'url' => '#'
        ),
        array(
          'title' => 'Finden Sie ein Geschäft',
          'url' => '#'
        ),
        array(
          'title' => 'Profi',
          'url' => '#'
        ),
        array(
          'title' => 'Marke und Technologie ',
          'url' => '#'
        ),
        array(
          'title' => 'und bietet Finanzierung',
          'url' => '#'
        ),
        array(
          'title' => 'Dienstleistungen und Zubehör',
          'url' => '#'
        )
      )
    )
  )
);
