<!DOCTYPE html>
<!--[if lt IE 8]><html lang="fr" class="ie ie7 no-js"><![endif]-->
<!--[if IE 8]><html lang="fr" class="ie ie8 no-js"><![endif]-->
<!--[if IE 9]><html lang="fr" class="ie ie9 no-js"><![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title>{$title|default:'Titre par défaut'}</title>
    <meta name="description" content="{$metaDescription|default:'Meta description par défaut'}">
    <meta name="keywords" content="{$metaKeywords|default:'Meta keywords par défaut'}">

    {$header|default:null}

    <link rel="stylesheet" href="{$cssUrl}/main.css">
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{$cssUrl}/main-ie.css">
    <![endif]-->
    <script type="text/javascript" src="{$jsUrl}/lib/modernizr.custom.js"></script>
</head>
<body>
    <nav class="slice-tp1">NAVIGATION</nav>
    <div class="body">
      {$body}
    </div>

    <!--[if gt IE 8]><!-->
    <script type="text/javascript" src="{$jsUrl}/lib/jquery.js"></script>
    <!--<![endif]-->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="{$jsUrl}/lib/ie/jquery.js"></script>
    <script type="text/javascript" src="{$jsUrl}/ie/scripts.js"></script>
    <![endif]-->

    <!-- build:js js/vendor.js -->
    <!-- endbuild -->
    <!-- build:js js/app.js -->
    <script type="text/javascript" src="{$jsUrl}/lib/polyfiller.js"></script>
    <script type="text/javascript" src="{$jsUrl}/lib/jquery.selectBox.js"></script>
    <script type="text/javascript" src="{$jsUrl}/lib/iso/core.js"></script>
    <script type="text/javascript" src="{$jsUrl}/lib/iso/forms.js"></script>
    <script type="text/javascript" src="{$jsUrl}/lib/iso/main.js"></script>
    <script type="text/javascript" src="{$jsUrl}/lib/iso/pt2.js"></script>
    <!-- endbuild -->


    {$footer|default:null}


</body>
</html>
